const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let colorButton = (colorList) => {
  let contentHTML = "";
  colorList.forEach((item) => {
    let content = `<button class="color-button ${item}"></button>`;
    contentHTML += content;
  });
  document.getElementById("colorContainer").innerHTML = contentHTML;
  document.querySelector(".color-button").classList.add("active");
  document.getElementById("house").classList.add(colorList[0]);
};
colorButton(colorList);

let getColor = (element) => {
  let elementColor = element[0].classList[1];
  console.log("elementColor: ", elementColor);
  return elementColor;
};
let changeColorHouse = (colorRemove, colorNew) => {
  houseColor = document.getElementById("house");
  houseColor.classList.replace(colorRemove, colorNew);
};
// bắt sự kiện click button bằng addEventListener
let btnContainer = document.getElementById("colorContainer");
let btns = btnContainer.getElementsByClassName("color-button");
for (let i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    let current = document.getElementsByClassName("active");
    console.log("current: ", current);
    let colorRemove = getColor(current);
    current[0].className = current[0].className.replace("active", "");
    this.className += " active";
    let colorNew = getColor(document.getElementsByClassName("active"));
    changeColorHouse(colorRemove, colorNew);
  });
}
