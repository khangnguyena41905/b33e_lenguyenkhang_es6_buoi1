let taoTheSpan = () => {
  let strEl = document.querySelector(".heading");
  let str = strEl.textContent;
  let strArr = [...str];

  let contentHTML = "";
  strArr.forEach((element) => {
    let content = `<span>${element}</span>`;
    contentHTML += content;
  });
  strEl.innerText = "";
  strEl.innerHTML = contentHTML;
};
taoTheSpan();
