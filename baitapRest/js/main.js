let kiemTraKhoi = (list) => {
  return list.length;
};
let tinhDTB = (diemMonThu1, diemMonThu2, diemMonThu3, ...diemMonThu4) => {
  if (kiemTraKhoi(diemMonThu4) == 0) {
    return (diemTb = (diemMonThu1 + diemMonThu2 + diemMonThu3) / 3);
  } else {
    return (diemTb =
      (diemMonThu1 + diemMonThu2 + diemMonThu3 + diemMonThu4[0]) / 4);
  }
};

let tinhDtbKhoi1 = () => {
  let diemMonThu1 = +document.getElementById("inpToan").value;
  let diemMonThu2 = +document.getElementById("inpLy").value;
  let diemMonThu3 = +document.getElementById("inpHoa").value;
  tinhDTB(diemMonThu1, diemMonThu2, diemMonThu3);
  document.getElementById("tbKhoi1").innerText = diemTb;
};
let tinhDtbKhoi2 = () => {
  let diemMonThu1 = +document.getElementById("inpVan").value;
  let diemMonThu2 = +document.getElementById("inpSu").value;
  let diemMonThu3 = +document.getElementById("inpDia").value;
  let diemMonThu4 = +document.getElementById("inpEnglish").value;
  tinhDTB(diemMonThu1, diemMonThu2, diemMonThu3, diemMonThu4);
  document.getElementById("tbKhoi2").innerText = diemTb;
};
